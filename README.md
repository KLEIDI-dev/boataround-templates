# README #

Boataround HTML/CSS template repository

## Installation ##

### Git access ###

> git clone https://Agoreddah@bitbucket.org/Agoreddah/boataround-templates.git

### Branches ###

> git fetch && git checkout dev

### Install NPM dependencies for the first time###

> sh install.sh

Run with administrator privilege, if facing issues with privileges.
If still faced with issues, manually execute the commands in installsh file

## Google Disk ##

### Google Disk access ###

https://drive.google.com/drive/folders/0B_OYpGo3cENjN2RRQXM1NC1tNDg?usp=sharing

### Manager of this repository ###

Tomas Chudjak
tomas@kleidi.sk