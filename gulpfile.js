var gulp = require("gulp");
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');

var appPath = "app/";
var libPath = appPath + "components/";
var assetsPath = appPath + "assets/";
var sassPath = assetsPath + "sass/";
var cssPath = assetsPath + "css/";
var jsPath = assetsPath + "js/";
var sassFile = "main.scss";

var port = 3001;
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        port: port,
        server: {
            baseDir: appPath
        }
    });

    gulp.watch(sassPath + "*.scss", ['sass']);
    gulp.watch(jsPath + "*.js").on('change', browserSync.reload);
    gulp.watch(appPath + "*.html").on('change', browserSync.reload);
});

gulp.task('sass', function() {
    return gulp.src(sassPath + sassFile)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(cssPath))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('default', ['serve']);
